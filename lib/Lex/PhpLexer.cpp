//===--- PhpLexer.cpp - PHP support for Lexer -----------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file implements part of the Lexer and Token interfaces necessary to
//  parse PHP sources.
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/CharInfo.h"
#include "clang/Lex/Lexer.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Basic/PhpDiagnostic.h"

using namespace clang;

static unsigned isEqualIgnoreCase(const char *Text, const char * Pattern) {
  const char *Current = Text;
  unsigned NumEqualChars = 0;
  while (*Current && *Pattern) {
    if (toLowercase(*Current) != toLowercase(*Pattern))
      return false;
    ++Current;
    ++Pattern;
    ++NumEqualChars;
  }
  if (*Pattern == 0)
    return NumEqualChars;
  return 0;
}


bool isStartOfInrepolationSequence(const char *CurPtr) {
  if (*CurPtr == '$' && (isIdentifierHead(CurPtr[1]) || CurPtr[1] == '{'))
    return true;
  if (*CurPtr == '{' && CurPtr[1] == '$')
    return true;
  return false;
}


/// \brief Tokenize input stream when the lexer interpolates string.
///
bool Lexer::lexPhpString(Token &Result, bool StartOfLine) {
  assert(PhpLexState == PhpState::Interpolation);

  // Quote in the interpolation mode stops processing string literal.
  if (*BufferPtr == '"') {
    PhpLexState = PhpState::PHP;
    return false;
  }

  // CurPtr - Cache BufferPtr in an automatic variable.
  const char *CurPtr = BufferPtr;

  // Determine string literal segment.
  for (; *CurPtr && *CurPtr != '\"'; ++CurPtr) {
    if (isStartOfInrepolationSequence(CurPtr)) {
      PhpLexState = PhpState::PHP;
      break;
    }
  }

  if (BufferPtr != CurPtr) {
    const char *Start = BufferPtr;
    FormTokenWithChars(Result, CurPtr, tok::string_literal);
    Result.setLiteralData(Start);
  }

  return Result.getKind() == tok::string_literal;
}


bool Lexer::lexHtmlData(Token &Result, bool StartOfLine) {
  assert(PhpLexState == PhpState::HTML);

  const char *Start = BufferPtr;

  // CurPtr - Cache BufferPtr in an automatic variable.
  const char *CurPtr = BufferPtr;

  // Read a character, advancing over it.
  const char *HtmlEnd = CurPtr;
  unsigned TagLen = 0;
  while (*CurPtr) {
    char Char = getAndAdvanceChar(CurPtr, Result);
    if (Char == '<') {
      // Possibly this is a tag that starts PHP code fragment.
      PhpTag OpeningTag = PhpTag::None;
      TagLen = checkStartOfPHP(CurPtr, OpeningTag);
      if (TagLen) {
        HtmlEnd = CurPtr - 1;
        ++TagLen; // account starting <
        ActivePhpTag = OpeningTag;
        PhpLexState = PhpState::PHP;
        break;
      }
    }
  }
  if (PhpLexState == PhpState::HTML) {
    // Reach end of text.
    HtmlEnd = CurPtr;
  }

  if (BufferPtr != HtmlEnd) {
    FormTokenWithChars(Result, HtmlEnd, tok::html_data);
    Result.setLiteralData(Start);
  }
  SkipBytes(TagLen, StartOfLine);
  return Result.getKind() == tok::html_data;
}

/// \brief Checks if the specified text starts an opening tag of php region.
/// \param CurPtr Points to the input text just after '<'.
/// \returns Length of the opening tag, counting from the second character.
unsigned Lexer::checkStartOfPHP(const char *CurPtr, PhpTag &Tag) {
  assert(CurPtr[-1] == '<');

  // There are several ways to start PHP code fragment:
  //
  // <?
  // <?php
  // <?=
  // <%
  // <script language='php'>
  unsigned TagLen = 0;
  switch (*CurPtr) {
  case '?':
    // <?php
    if (unsigned NumChars = isEqualIgnoreCase(CurPtr + 1, "php")) {
      if (CurPtr[NumChars + 1] == 0 || !isIdentifierBody(CurPtr[NumChars + 1])) {
        TagLen = 1 + NumChars;
        Tag = PhpTag::PHP;
      }
      break;
    }
    // <?=
    if (*(CurPtr+1) == '=') {
      TagLen = 2;
      Tag = PhpTag::Echo;
      break;
    }
    // <?
    TagLen = 1;
    Tag = PhpTag::Short;
    break;

  case '%':
    // <%
    TagLen = 1;
    Tag = PhpTag::ASP;
    break;

  case 's':
  case 'S': {
    // <script language = php>
    const char *Cursor = CurPtr;
    unsigned NumChars;

    NumChars = isEqualIgnoreCase(Cursor, "script");
    if (!NumChars)
      return false;
    Cursor += NumChars;
    // Skip whitespaces
    while (isWhitespace(*Cursor))
      ++Cursor;

    NumChars = isEqualIgnoreCase(Cursor, "language");
    if (!NumChars)
      return false;
    Cursor += NumChars;
    while (isWhitespace(*Cursor))
      ++Cursor;

    if (*Cursor != '=')
      return false;
    ++Cursor;
    while (isWhitespace(*Cursor))
      ++Cursor;

    // Keep kind of quotes
    enum { Single, Double, None} QuoteKind;
    switch (*Cursor) {
    case '\'': QuoteKind = Single; ++Cursor; break;
    case '\"': QuoteKind = Double; ++Cursor; break;
    default: QuoteKind = None;
    }
    NumChars = isEqualIgnoreCase(Cursor, "php");
    if (!NumChars)
      return false;
    Cursor += NumChars;
    switch (QuoteKind) {
    case Single:
      if (*Cursor != '\'')
        return false;
      ++Cursor;
      break;
    case Double:
      if (*Cursor != '\"')
        return false;
      ++Cursor;
      break;
    default:
      break;
    }
    while (isWhitespace(*Cursor))
      ++Cursor;

    if (*Cursor != '>')
      return false;
    ++Cursor;
    TagLen = Cursor - CurPtr;
    break;
  }
  default:
    return false;
  }

  return TagLen;
}

/// \brief Checks if the specified text starts a closing tag of php region if so
///        updates the lexer state properly.
/// \param Result Token that is set if the closing tag is immediately followed
///               by another open tag.
/// \param CurPtr Points to the input text.
/// \returns True if closing tag was found.
bool Lexer::checkEndOfPHP(Token &Result, const char *CurPtr) {

  // Check if just read character starts closing tag.
  unsigned TagLen = 0;
  PhpTag Tag = PhpTag::None;
  switch (CurPtr[- 1]) {
  case '?':
  case '%':
    if (*CurPtr == '>') {
      TagLen = 1;
      Tag = (CurPtr[- 1] == '?') ? PhpTag::PHP : PhpTag::ASP;
    }
    break;
  case '<':
    TagLen = isEqualIgnoreCase(CurPtr, "/script>");
    if (TagLen)
      Tag = PhpTag::Script;
    break;
  default:
    break;
  }

  if (Tag == PhpTag::None)
    return false;

  // Check tag mismatch.
  switch (ActivePhpTag) {
  case PhpTag::PHP:
  case PhpTag::Echo:
  case PhpTag::Short:
    if (Tag == PhpTag::PHP)
      break;
  default:
    if (Tag != ActivePhpTag)
      Diag(CurPtr - 1, diag::warn_php_tag_mismatch);
    break;
  }

  // Update lexer state.
  SkipBytes(TagLen + 1, false);
  PhpLexState = PhpState::HTML;
  ActivePhpTag = PhpTag::None;
  CurPtr += TagLen;

  // Check if this PHP region is immediately followed by the start of another
  // PHP region. In this case we need to create empty html_data. It is needed
  // because a statement may be be terminated by subsequent php closing tag
  // rather than a semicolon.
  PhpTag NextTag = PhpTag::None;
  if (*CurPtr == '<' && checkStartOfPHP(CurPtr + 1, NextTag) > 0) {
    assert(Result.is(tok::unknown));
    FormTokenWithChars(Result, CurPtr, tok::html_data);
  }

  return true;
}


/// \brief Restores lexer current position to previously the previously saved.
///
void Lexer::restoreInterpolationMode() {
  assert(PreviousBuffer);
  assert(PhpLexState == PhpState::PHP);
  PhpLexState = PhpState::Interpolation;
  BufferPtr = PreviousBuffer;
  PreviousBuffer = nullptr;
}


tok::TokenKind Lexer::handleQuote(char current_quote) {
  // Outer quote is the quote used at the beggining of string literal
  // Inner quote is used inside string literal

  // "`s`" - in this example: 
  // " is outer quote
  // ` is inner quote

  // If lexer find closing outer quote string liteteral is terminated 
  // regardless of state of inner quote  

  tok::TokenKind Kind;
  switch (FirstLevelQuote) {
  // No outer quote has been seen before
  case 0:
    FirstLevelQuote = current_quote;
    // There can't be inner quote without outer quote
    assert(!SecondLevelQuote);
    Kind = current_quote == '"' ? tok::quote : tok::backquote;
    setInterpolationMode();
    break;
  case '"':
  case '`':
    // Current quote matches outer quote
    // we emmit closing_quote token and switch
    // to phpMode as string has finished
    // SeondLevelQuote is reset
    if (FirstLevelQuote == current_quote) {
      FirstLevelQuote = 0;
      SecondLevelQuote = 0;
      Kind = current_quote == '"' ? tok::closing_quote : tok::closing_backquote;
      setPhpMode();
      break;
    }
    // Current quote does not match outer quote. 
    // We need to check if have seen opening 
    // inner quote before and act accordingly
    else {
      switch (SecondLevelQuote) {
        // No opening inner quote set
      case 0:
        // Inner quote without outer quote makes no sense
        assert(FirstLevelQuote);
        // Emmit opening quote but do not switch lexer mode
        // as we should already be in interpolation mode
        SecondLevelQuote = '"';
        Kind = tok::quote;
        break;
      case '"':
      case '`':
        // There has been opening inner quote
        // we should emmit closing quote token
        // but without switching php mode
        if (current_quote == SecondLevelQuote) {
          Kind = current_quote == '"' ? tok::closing_quote : tok::closing_backquote;
          // Reset inner quote
          SecondLevelQuote = 0;
          break;
        }
        else {
          // This should not be possible
          assert(0 && "Invalid quotes sequence");
          break;
        }
      }
    }
  }
  return Kind;
}