//===--- DeclPhp.cpp - PHP Declaration AST Node Implementation ------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the PHP statement subclasses.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
//------------------------------------------------------------------------------

namespace clang {

//==============================================================================
// Implementation of PhpEchoStmt
//==============================================================================


PhpEchoStmt::PhpEchoStmt(ASTContext &C, SourceLocation KWLoc,
                         ArrayRef<Expr *> Items, bool IsHtmlData)
  : Stmt(PhpEchoStmtClass), EchoLoc(KWLoc), IsHtmlData(IsHtmlData) {
  assert(!Items.empty());
  Arguments = new (C) Stmt*[Items.size()];
  NumArguments = Items.size();
  std::copy(Items.begin(), Items.end(), Arguments);
}


PhpEchoStmt::PhpEchoStmt(EmptyShell Empty)
  : Stmt(PhpEchoStmtClass, Empty), Arguments(nullptr), NumArguments(0) {
}


PhpEchoStmt *PhpEchoStmt::Create(ASTContext &Context,
                                 SourceLocation KWLoc,
                                 ArrayRef<Expr *> Items,
                                 bool IsHtmlData) {
  PhpEchoStmt *S = new (Context) PhpEchoStmt(Context, KWLoc, Items, IsHtmlData);
  return S;
}


void PhpEchoStmt::setArgs(ASTContext &C, Expr **content, unsigned num) {
  assert(content && num);
  if (Arguments)
    C.Deallocate(Arguments);
  NumArguments = num;

  Arguments = new (C) Stmt*[num];
  memcpy(Arguments, content, sizeof(Stmt *) * num);
}


//==============================================================================
// Implementation of PhpFunctionDefStmt
//==============================================================================

PhpFunctionDefStmt *PhpFunctionDefStmt::Create(ASTContext &Context,
                                               FunctionDecl *FDecl) {
  assert(FDecl);
  PhpFunctionDefStmt *S = new (Context)PhpFunctionDefStmt(FDecl);
  return S;
}


PhpFunctionDefStmt::PhpFunctionDefStmt(EmptyShell Empty)
  : Stmt(PhpEchoStmtClass, Empty), FDecl(nullptr) {
}


PhpFunctionDefStmt::PhpFunctionDefStmt(FunctionDecl *FDecl)
  : Stmt(PhpFunctionDefStmtClass), FDecl(FDecl) {
}

}
