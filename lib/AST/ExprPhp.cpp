//===--- ExprPhp.cpp - PHP Declaration AST Node Implementation ------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the PHP expression subclasses.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/ExprPhp.h"
#include "clang/AST/DeclCXX.h"
//------------------------------------------------------------------------------

namespace clang {

//==============================================================================
// Implementation of PhpMapExpr
//==============================================================================

PhpMapExpr::PhpMapExpr(SourceLocation ArrowLoc, Expr *LHS, Expr *RHS)
  : Expr(PhpMapExprClass, QualType(), VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true,
  /*ContainsUnexpandedParameterPack*/ false),
  ArrowLoc(ArrowLoc) {
  SubExprs[0] = LHS;
  SubExprs[1] = RHS;
}


//==============================================================================
// Implementation of PhpArrayExpr
//==============================================================================

PhpArrayExpr::PhpArrayExpr(ASTContext &C, QualType T, SourceLocation KW,
             SourceLocation LBLoc, ArrayRef<Expr *> Items, SourceLocation RBLoc)
  : Expr(PhpArrayExprClass, T, VK_RValue, OK_Ordinary,
         /*Type dependent*/ false, /*Value dependent*/ false,
         /*Instantiation dependent*/ false,
         /*ContainsUnexpandedParameterPack*/ false),
    KwLoc(KW), LBLoc(LBLoc), RBLoc(RBLoc) {
  NumElements = Items.size();
  if (!Items.empty()) {
    Elements = new (C) Stmt*[Items.size()];
    std::copy(Items.begin(), Items.end(), Elements);
  }
}


PhpArrayExpr *PhpArrayExpr::Create(ASTContext &Context,
                                   QualType T,
                                   SourceLocation KW,
                                   SourceLocation LBLoc,
                                   ArrayRef<Expr *> Items,
                                   SourceLocation RBLoc) {
  PhpArrayExpr *E = new (Context) PhpArrayExpr(Context, T, KW,
                                               LBLoc, Items, RBLoc);
  return E;
}


PhpArrayExpr::PhpArrayExpr(EmptyShell Empty)
  : Expr(PhpArrayExprClass, Empty), NumElements(0) {
}


void PhpArrayExpr::setElements(ASTContext &C, Expr **content, unsigned num) {
  NumElements = num;
  if (num > 0) {
    Elements = new (C) Stmt*[num];
    std::copy(content, content + num, Elements);
  }
}


//==============================================================================
// Implementation of PhpSuppressExpr
//==============================================================================

PhpSuppressExpr::PhpSuppressExpr(SourceLocation SuppressLoc, Expr *SubExpr)
  : Expr(PhpSuppressExprClass, SubExpr->getType(), VK_RValue, OK_Ordinary,
         /*Dependent*/ true, true, true,
         /*ContainsUnexpandedParameterPack*/ false), SubExpr(SubExpr),
         SuppressLoc(SuppressLoc) {
}


//==============================================================================
// Implementation of PhpNewExpr
//==============================================================================

PhpNewExpr::PhpNewExpr(ASTContext &C, SourceLocation NewLoc,
                       CXXRecordDecl *Class, SourceLocation LP,
                       ArrayRef<Expr *> Args, SourceLocation RP)
  : Expr(PhpNewExprClass, C.getTypeDeclType(Class), VK_RValue, OK_Ordinary,
         /*Dependent*/ true, true, true,
         /*ContainsUnexpandedParameterPack*/ false),
    Class(Class), NewLoc(NewLoc), LParen(LP), RParen(RP) {
  setArgs(C, Args.data(), Args.size());
}


PhpNewExpr::PhpNewExpr(ASTContext &C, QualType Ty, SourceLocation NewLoc,
                       Expr *Name, SourceLocation LP,
                       ArrayRef<Expr *> Args, SourceLocation RP)
  : Expr(PhpNewExprClass, Ty, VK_RValue, OK_Ordinary,
         /*Dependent*/ true, true, true,
         /*ContainsUnexpandedParameterPack*/ false),
    Class(nullptr), NewLoc(NewLoc), LParen(LP), RParen(RP) {
  setArgs(C, Args.data(), Args.size());
  setClassName(Name);
}


void PhpNewExpr::setArgs(ASTContext &C, Expr *const *content, unsigned num) {
  NumArgs = num;
  Args = new (C) Stmt*[PREARGS_START + num];
  if (num > 0)
    std::copy(content, content + num, Args);
  Args[ClassName] = nullptr;
}


SourceLocation PhpNewExpr::getLocEnd() const {
  if (RParen.isValid())
  return RParen;
  if (Class)
    return Class->getLocEnd();
  assert(getClassNameExpr());
  return getClassNameExpr()->getLocEnd();
}


//==============================================================================
// Implementation of PhpCloneExpr
//==============================================================================

PhpCloneExpr::PhpCloneExpr(SourceLocation CloneLoc, Expr *SubExpr) 
  : Expr(PhpCloneExprClass, SubExpr->getType(), VK_RValue, OK_Ordinary,
         /*Dependent*/ true, true, true,
         /*ContainsUnexpandedParameterPack*/ false),
  SubExpr(SubExpr), CloneLoc(CloneLoc) {
}


//==============================================================================
// Implementation of PhpNullLiteral
//==============================================================================

PhpNullLiteral::PhpNullLiteral(QualType Ty, SourceLocation Loc)
  : Expr(PhpNullLiteralClass, Ty, VK_RValue, OK_Ordinary,
  /*Dependent*/ false, false, false,
  /*ContainsUnexpandedParameterPack*/ false), Loc(Loc) {
}


//==============================================================================
// Implementation of PhpInstanceofExpr
//==============================================================================

PhpInstanceofExpr::PhpInstanceofExpr(ASTContext &C, SourceLocation Loc,
  Expr *Base, Expr *ClassName)
  : Expr(PhpInstanceofExprClass, C.BoolTy, VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true, /*ContainsUnexpandedParameterPack*/ false),
  ClassNameDecl(nullptr), Loc(Loc) {
  Exprs[0] = Base;
  Exprs[1] = ClassName;
}

PhpInstanceofExpr::PhpInstanceofExpr(ASTContext &C, SourceLocation Loc,
  Expr *Base, CXXRecordDecl *ClassName)
  : Expr(PhpInstanceofExprClass, C.BoolTy, VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true, /*ContainsUnexpandedParameterPack*/ false),
  ClassNameDecl(ClassName), Loc(Loc) {
  Exprs[0] = Base;
  Exprs[1] = nullptr;
}


//==============================================================================
// Implementation of PhpIncludeExpr
//==============================================================================

PhpIncludeExpr::PhpIncludeExpr(QualType Ty, SourceLocation Loc,
  IncludeKind Kind, Expr *FileName)
  : Expr(PhpIncludeExprClass, Ty, VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true, /*ContainsUnexpandedParameterPack*/ false),
  FileName(FileName), Kind(Kind), Loc(Loc) {
}


//==============================================================================
// Implementation of PhpEllipsisExpandExpr
//==============================================================================

PhpEllipsisExpandExpr::PhpEllipsisExpandExpr(QualType Ty,
  SourceLocation EllipsisLoc, Expr *Base)
  : Expr(PhpEllipsisExpandExprClass, Ty, VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true, /*ContainsUnexpandedParameterPack*/ false),
  Expand(Base), Loc(EllipsisLoc) {
}


//==============================================================================
// Implementation of PhpReferenceExpr
//==============================================================================

PhpReferenceExpr::PhpReferenceExpr(SourceLocation ReferenceLoc, Expr *Base)
  : Expr(PhpReferenceExprClass, Base->getType(), VK_RValue, OK_Ordinary,
  /*Dependent*/ true, true, true, /*ContainsUnexpandedParameterPack*/ false),
  Base(Base), Loc(ReferenceLoc) {
}

}
