//===--- PhpSupport.cpp - PHP support in driver ---------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Driver/Driver.h"
#include "llvm/Option/ArgList.h"
#include "llvm/Option/Option.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/StringSaver.h"
#include "clang/Driver/PhpSupport.h"
#include "clang/Basic/Version.h"
//------------------------------------------------------------------------------


namespace clang { namespace driver {

using namespace clang;
using namespace llvm::opt;


std::string getPhpResourcePath(std::string ClangResourceDir) {
  SmallString<128> PhlangArgsFile;
  llvm::sys::path::append(PhlangArgsFile, ClangResourceDir, "..", "..");
  llvm::sys::path::append(PhlangArgsFile, "phpfe", PHPFE_VERSION_STRING);
  return PhlangArgsFile.str();
}


void addPhpLinkerOptions(const Driver &D, const ArgList &Args,
                         ArgStringList &CmdArgs, bool IsWin) {
  if (IsWin) {
    CmdArgs.push_back("/SUBSYSTEM:CONSOLE");
    CmdArgs.push_back("/DEFAULTLIB:MSVCRTD");
    CmdArgs.push_back("/INCREMENTAL:NO");
  }
}


void addPhpLinkerLibs(const Driver &D, const ArgList &Args,
                      ArgStringList &CmdArgs, bool IsWin) {
  llvm::BumpPtrAllocator A;
  llvm::StringSaver Saver(A);
  llvm::cl::TokenizerCallback Tokenizer = &llvm::cl::TokenizeGNUCommandLine;

  SmallVector<const char *, 256> ExtraArgs;
  SmallString<128> PhlangArgsFile;
  llvm::sys::path::append(PhlangArgsFile, getPhpResourcePath(D.ResourceDir));
  if (IsWin) {
    CmdArgs.push_back(Args.MakeArgString(Twine("/LIBPATH:").concat(PhlangArgsFile)));
  } else {
    CmdArgs.push_back("-L");
    CmdArgs.push_back(Args.MakeArgString(PhlangArgsFile));
    //TODO: add this library only if it is actually present.
    CmdArgs.push_back("-ltinfo");
  }

  llvm::sys::path::append(PhlangArgsFile, "phlang_args.txt");
  ExtraArgs.push_back(Args.MakeArgString(Twine('@').concat(PhlangArgsFile)));
  llvm::cl::ExpandResponseFiles(Saver, Tokenizer, ExtraArgs, false);

  for (auto Arg : ExtraArgs)
    CmdArgs.push_back(Args.MakeArgString(Arg));
}

} }



