This repository is a part of [phlang project](https://bitbucket.org/php-llvm/phlang). It contains modified version of clang adapted for working with PHP frontend.

See [http://clang.llvm.org](http://clang.llvm.org) for original version.