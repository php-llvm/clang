//===--- StmtPhp.h - Classes for representing PHP statements ----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the PHP statement AST node classes.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_AST_STMTPHP_H
#define LLVM_CLANG_AST_STMTPHP_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ExprCXX.h"
#include "clang/AST/Stmt.h"
#include "clang/Basic/SourceLocation.h"
//------------------------------------------------------------------------------


namespace clang {

class PhpEchoStmt : public Stmt {
  SourceLocation EchoLoc;
  Stmt **Arguments;
  unsigned NumArguments;
  bool IsHtmlData;

  PhpEchoStmt(ASTContext &C, SourceLocation KWLoc, ArrayRef<Expr *> Items, 
              bool IsHtmlData);

  friend class ASTStmtReader;

public:
  static PhpEchoStmt *Create(ASTContext &Context,
                             SourceLocation KWLoc,
                             ArrayRef<Expr *> Items,
                             bool IsHtmldata = false);

  PhpEchoStmt(EmptyShell Empty);

  SourceLocation getEchoLoc() const { return EchoLoc; }
  void setEchoLoc(SourceLocation Loc) { EchoLoc = Loc; }

  bool isHtmlData() const { return IsHtmlData; }

  unsigned getNumArgs() const { return NumArguments; }
  Expr *getArg(unsigned Arg) {
    assert(Arg < NumArguments);
    return cast<Expr>(Arguments[Arg]);
  }
  const Expr *getArg(unsigned Arg) const {
    assert(Arg < NumArguments);
    return cast<Expr>(Arguments[Arg]);
  }
  Expr **getArgs() {
    return reinterpret_cast<Expr **>(Arguments);
  }
  void setArgs(ASTContext &C, Expr **content, unsigned num);

  //------ Container access to subexpressions

  typedef ExprIterator arg_iterator;
  typedef ConstExprIterator const_arg_iterator;
  typedef llvm::iterator_range<arg_iterator> arg_range;
  typedef llvm::iterator_range<const_arg_iterator> arg_const_range;

  arg_iterator arg_begin() { return Arguments; }
  arg_iterator arg_end() { return Arguments + NumArguments; }
  const_arg_iterator arg_begin() const { return Arguments; }
  const_arg_iterator arg_end() const { return Arguments + NumArguments; }

  arg_range arguments() { return arg_range(arg_begin(), arg_end()); }
  arg_const_range arguments() const {
    return arg_const_range(arg_begin(), arg_end());
  }

  //------ Stmt interface

  SourceLocation getLocStart() const LLVM_READONLY { return EchoLoc; }
  SourceLocation getLocEnd() const LLVM_READONLY {
    assert(Arguments && NumArguments);
    return Arguments[NumArguments - 1]->getLocEnd();
  }
  SourceRange getSourceRange() const LLVM_READONLY {
    return SourceRange(EchoLoc, getLocEnd());
  }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpEchoStmtClass;
  }

  // Iterators
  child_range children() {
    assert(Arguments && NumArguments);
    return child_range(Arguments, Arguments + NumArguments);
  }
  const_child_range children() const {
    assert(Arguments && NumArguments);
    child_range Children = const_cast<PhpEchoStmt *>(this)->children();
    return const_child_range(Children.begin(), Children.end());
  }
};


/// \brief Statement that introduces function definition in runtime.
///
class PhpFunctionDefStmt : public Stmt {
  FunctionDecl *FDecl;

  PhpFunctionDefStmt(FunctionDecl *FDecl);

public:
  static PhpFunctionDefStmt *Create(ASTContext &Context,
                                    FunctionDecl *FDecl);

  PhpFunctionDefStmt(EmptyShell Empty);

  FunctionDecl *getFunction() const { return FDecl; }
  void setFunction(FunctionDecl *FD) { FDecl = FD; }

  //------ Stmt interface

  SourceLocation getLocStart() const LLVM_READONLY {
    return FDecl->getLocStart();
  }

  SourceLocation getLocEnd() const LLVM_READONLY {
    assert(FDecl);
    return FDecl->getLocEnd();
  }

  SourceRange getSourceRange() const LLVM_READONLY {
    return SourceRange(getLocStart(), getLocEnd());
  }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpFunctionDefStmtClass;
  }

  // Iterators
  child_range children() {
    return child_range(child_iterator(), child_iterator()); 
  }
};


}

#endif
