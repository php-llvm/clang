//===--- ExprPhp.h - Classes for representing expressions -------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief Defines the PHP expression AST node classes.
///
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_AST_EXPRPHP_H
#define LLVM_CLANG_AST_EXPRPHP_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/Expr.h"
//------------------------------------------------------------------------------

namespace clang {

/// \brief Represent elements like idx => val.
///
class PhpMapExpr : public Expr {
  Stmt *SubExprs[2];
  SourceLocation ArrowLoc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setArrowLoc(SourceLocation L) { ArrowLoc = L; }

public:
  PhpMapExpr(SourceLocation ArrowLoc, Expr *LHS, Expr *RHS);
  PhpMapExpr(EmptyShell Empty) : Expr(PhpMapExprClass, Empty) {}

  Expr *getLHS() const { return static_cast<Expr*>(SubExprs[0]); }
  Expr *getRHS() const { return static_cast<Expr*>(SubExprs[1]); }

  void setLHS(Expr *E) { SubExprs[0] = E; }
  void setRHS(Expr *E) { SubExprs[1] = E; }

  SourceLocation getArrowLoc() const LLVM_READONLY { return ArrowLoc; }
  SourceLocation getLocStart() const LLVM_READONLY {
    return getLHS()->getLocStart();
  }
  SourceLocation getLocEnd() const LLVM_READONLY {
    return getRHS()->getLocEnd();
  }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpMapExprClass;
  }

  // Iterators
  child_range children() { return child_range(SubExprs, SubExprs + 2); }
};


/// \brief Represents PHP array.
///
class PhpArrayExpr : public Expr {
  Stmt **Elements;
  unsigned NumElements;
  SourceLocation KwLoc;   //> Location of keyword 'array', if specified.
  SourceLocation LBLoc;   //> Opening bracket, '(' or '['.
  SourceLocation RBLoc;   //> Closing bracket.

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  PhpArrayExpr(ASTContext &C, QualType T, SourceLocation KW,
            SourceLocation LBLoc, ArrayRef<Expr *> Items, SourceLocation RBLoc);

  void setKwLoc(SourceLocation Loc) { KwLoc = Loc; }
  void setLBLoc(SourceLocation Loc) { LBLoc = Loc; }
  void setRBLoc(SourceLocation Loc) { RBLoc = Loc; }

public:
  static PhpArrayExpr *Create(ASTContext &Context,
                              QualType T,
                              SourceLocation KW,
                              SourceLocation LBLoc,
                              ArrayRef<Expr *> Items,
                              SourceLocation RBLoc);

  PhpArrayExpr(EmptyShell Empty);

  SourceLocation getKwLoc() const { return KwLoc; }
  SourceLocation getLBLoc() const { return LBLoc; }
  SourceLocation getRBLoc() const { return RBLoc; }

  unsigned getNumElements() const { return NumElements; }

  Expr *getElement(unsigned I) {
    assert(I < NumElements);
    return cast<Expr>(Elements[I]);
  }

  const Expr *getElement(unsigned I) const {
    assert(I < NumElements);
    return cast<Expr>(Elements[I]);
  }

  void setElement(unsigned I, Expr *E) {
    assert(I < NumElements);
    Elements[I] = E;
  }

  Expr **getElements() { return reinterpret_cast<Expr **>(Elements); }

  void setElements(ASTContext &C, Expr **content, unsigned num);

  //------ Container access to subexpressions

  typedef ExprIterator item_iterator;
  typedef ConstExprIterator const_item_iterator;
  typedef llvm::iterator_range<item_iterator> item_range;
  typedef llvm::iterator_range<const_item_iterator> item_const_range;

  item_iterator item_begin() { return Elements; }
  item_iterator item_end() { return Elements + NumElements; }
  const_item_iterator item_begin() const { return Elements; }
  const_item_iterator item_end() const { return Elements + NumElements; }

  item_range items() { return item_range(item_begin(), item_end()); }
  item_const_range items() const {
    return item_const_range(item_begin(), item_end());
  }

  //------ Stmt interface

  SourceLocation getLocStart() const LLVM_READONLY {
    if (KwLoc.isValid())
      return KwLoc;
    return LBLoc;
  }
  SourceLocation getLocEnd() const LLVM_READONLY   { return RBLoc; }
  SourceRange getSourceRange() const LLVM_READONLY {
    return SourceRange(getLocStart(), getLocEnd());
  }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpArrayExprClass;
  }

  // Iterators
  child_range children() {
    return child_range(Elements, Elements + NumElements);
  }
  const_child_range children() const {
    child_range Children = const_cast<PhpArrayExpr *>(this)->children();
    return const_child_range(Children.begin(), Children.end());
  }
};


/// \brief Represents @expr
///
class PhpSuppressExpr : public Expr {
  Stmt *SubExpr;
  SourceLocation SuppressLoc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setSuppressLoc(SourceLocation L) { SuppressLoc = L; }

public:
  PhpSuppressExpr(SourceLocation SuppressLoc, Expr *SubExpr);
  PhpSuppressExpr(EmptyShell Empty) : Expr(PhpSuppressExprClass, Empty) {}

  Expr *getSubExpr() const { return static_cast<Expr*>(SubExpr); }
  void setSubExpr(Expr *E) { SubExpr = E; }

  SourceLocation getSuppressLoc() const LLVM_READONLY { return SuppressLoc; }
  SourceLocation getLocStart() const LLVM_READONLY { return SuppressLoc; }
    SourceLocation getLocEnd() const LLVM_READONLY {
    return SubExpr->getLocEnd();
  }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpSuppressExprClass;
  }

  // Iterators
  child_range children() { return child_range(&SubExpr, &SubExpr + 1); }
};


/// \brief represents PHP new expression.
///
/// PHP new expression must have a class name an optionally argument list for
/// constructor.
///
/// Examples:
///     new ABC;
///     new ABC(1, $x);
///     new $a;
///     new $a(0, 2);
///
class PhpNewExpr : public Expr {
  enum { ClassName = 0, PREARGS_START = 1 };

  Stmt **Args;            //> Class name (if dynamic) and arguments of
                          //> constructor if present.
  unsigned NumArgs;       //> Number of constructor arguments.
  CXXRecordDecl *Class;  //> Class declaration if
                          // resolved, class name expression otherwise.
  SourceLocation NewLoc;  //> Location of 'new'.
  SourceLocation LParen,  //> Left paren of constructor argument.
                 RParen;  //> Right paren.

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setNewLoc(SourceLocation Loc) { NewLoc = Loc; }

public:
  PhpNewExpr(ASTContext &C, SourceLocation NewLoc,
             CXXRecordDecl *Class, SourceLocation LP, ArrayRef<Expr *> Args,
             SourceLocation RP);

  PhpNewExpr(ASTContext &C, QualType Ty, SourceLocation NewLoc,
             Expr *ClassName, SourceLocation LP, ArrayRef<Expr *> Args,
             SourceLocation RP);

  PhpNewExpr(EmptyShell Empty) : Expr(PhpNewExprClass, Empty), Args(nullptr),
    NumArgs(0), Class(nullptr) {}

  SourceLocation getNewLoc() const { return NewLoc; }

  //------ Access to constructor arguments

  unsigned getNumArgs() const { return NumArgs; }

  Expr *getArg(unsigned I) {
    assert(I < NumArgs);
    return cast<Expr>(Args[PREARGS_START + I]);
  }

  const Expr *getArg(unsigned I) const {
    assert(I < NumArgs);
    return cast<Expr>(Args[PREARGS_START + I]);
  }

  Expr **getArgs() {
    return reinterpret_cast<Expr **>(Args + PREARGS_START);
  }

  void setArgs(ASTContext &C, Expr *const *content, unsigned num);

  //------ Access to class name

  bool isClassResolved() const { return Class != nullptr; }
  bool isClassDynamic() const { return Class == nullptr; }

  CXXRecordDecl *getClassDecl() {
    assert(isClassResolved());
    return Class;
  }
  void setClassDecl(CXXRecordDecl *RD) {
    Class = RD;
  }

  Expr *getClassNameExpr() const {
    assert(isClassDynamic());
    return static_cast<Expr *>(Args[ClassName]);
  }
  void setClassName(Expr *Name) {
    assert(Class == nullptr);
    Args[ClassName] = Name;
  }

  //------ Container access to subexpressions

  typedef ExprIterator arg_iterator;
  typedef ConstExprIterator const_arg_iterator;
  typedef llvm::iterator_range<arg_iterator> arg_range;
  typedef llvm::iterator_range<const_arg_iterator> arg_const_range;

  arg_iterator arg_begin() { return Args + PREARGS_START; }
  arg_iterator arg_end() { return Args + PREARGS_START + NumArgs; }
  const_arg_iterator arg_begin() const { return Args + PREARGS_START; }
  const_arg_iterator arg_end() const { return Args + PREARGS_START + NumArgs; }

  arg_range arguments() { return arg_range(arg_begin(), arg_end()); }
  arg_const_range arguments() const {
    return arg_const_range(arg_begin(), arg_end());
  }

  //------ Stmt interface

  SourceLocation getLocStart() const LLVM_READONLY { return NewLoc; }
  SourceLocation getLocEnd() const LLVM_READONLY;
  SourceRange getSourceRange() const LLVM_READONLY {
    return SourceRange(getLocStart(), getLocEnd());
  }

    static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpNewExprClass;
  }

  // Iterators
  child_range children() {
    return child_range(Args, Args + PREARGS_START + NumArgs);
  }
  const_child_range children() const {
    child_range Children = const_cast<PhpNewExpr *>(this)->children();
    return const_child_range(Children.begin(), Children.end());
  }
};


/// \brief Represents PHP clone <expr>.
///
class PhpCloneExpr : public Expr {
  Stmt *SubExpr;
  SourceLocation CloneLoc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setCloneLoc(SourceLocation L) { CloneLoc = L; }

public:
  PhpCloneExpr(SourceLocation CloneLoc, Expr *SubExpr);
  PhpCloneExpr(EmptyShell Empty) : Expr(PhpCloneExprClass, Empty) {}

  Expr *getSubExpr() const { return static_cast<Expr*>(SubExpr); }
  void setSubExpr(Expr *E) { SubExpr = E; }

  SourceLocation getCloneLoc() const LLVM_READONLY { return CloneLoc; }
  SourceLocation getLocStart() const LLVM_READONLY { return CloneLoc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return SubExpr->getLocEnd(); }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpCloneExprClass;
  }

  // Iterators
  child_range children() { return child_range(&SubExpr, &SubExpr + 1); }
};


/// \brief Represents PHP null value.
///
class PhpNullLiteral : public Expr {
  SourceLocation Loc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setNullLoc(SourceLocation L) { Loc = L; }

public:
  PhpNullLiteral(QualType Ty, SourceLocation Loc);
  PhpNullLiteral(EmptyShell Empty) : Expr(PhpNullLiteralClass, Empty) {}

  SourceLocation getNullLoc() const LLVM_READONLY { return Loc; }
  SourceLocation getLocStart() const LLVM_READONLY { return Loc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return Loc; }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpNullLiteralClass;
  }

  child_range children() {
    return child_range(child_iterator(), child_iterator());
  }
};


/// \brief Represents PHP include expression.
///
class PhpIncludeExpr : public Expr {
public:
  enum IncludeKind {
    Include,
    IncludeOnce,
    Require,
    RequireOnce
  };

private:
  Stmt *FileName;
  IncludeKind Kind;
  SourceLocation Loc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setIncludeLoc(SourceLocation L) { Loc = L; }

public:
  PhpIncludeExpr(QualType Ty, SourceLocation Loc, 
                 IncludeKind Kind, Expr *FileName);
  PhpIncludeExpr(EmptyShell Empty) : Expr(PhpIncludeExprClass, Empty),
    FileName(nullptr) {}

  Expr *getFileName() const { return static_cast<Expr*>(FileName); }
  void setFileName(Expr *E) { FileName = E; }

  IncludeKind getIncludeKind() const { return Kind; }
  void setIncludeKind(IncludeKind Kind) { this->Kind = Kind; }

  SourceLocation getIncludeLoc() const LLVM_READONLY { return Loc; }
  SourceLocation getLocStart() const LLVM_READONLY { return Loc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return FileName->getLocEnd(); }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpIncludeExprClass;
  }

  child_range children() {
    return child_range(&FileName, &FileName + 1);
  }
};


/// \brief Represents PHP instanceof expression.
///
/// <base-expr> instanceof <class-expr>
///
class PhpInstanceofExpr : public Expr {
  Stmt *Exprs[2];
  CXXRecordDecl *ClassNameDecl;
  SourceLocation Loc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setInstanceofLoc(SourceLocation L) { Loc = L; }

public:
  PhpInstanceofExpr(ASTContext &C, SourceLocation Loc, 
                    Expr *Base, Expr *ClassName);
  PhpInstanceofExpr(ASTContext &C, SourceLocation Loc, 
                    Expr *Base, CXXRecordDecl *ClassName);
  PhpInstanceofExpr(EmptyShell Empty) : Expr(PhpInstanceofExprClass, Empty), 
    ClassNameDecl(nullptr) {
    Exprs[0] = Exprs[1] = nullptr;
  }

  Expr *getBase() const { return static_cast<Expr*>(Exprs[0]); }
  void setBase(Expr *E) { Exprs[0] = E; }

  bool classNameIsDecl() const { return ClassNameDecl != nullptr; }
  bool classNameIsExpr() const { return Exprs[1] != nullptr; }

  Expr *getClassNameExpr() {
    assert(classNameIsExpr());
    return static_cast<Expr*>(Exprs[1]);
  }
  
  void setClassNameExpr(Expr *Name) {
    assert(!classNameIsDecl());
    Exprs[1] = Name;
  }

  CXXRecordDecl *getClassNameDecl() {
    assert(classNameIsDecl());
    return ClassNameDecl;
  }

  void setClassNameDecl(CXXRecordDecl *Name) {
    assert(!classNameIsExpr());
    ClassNameDecl = Name;
  }

  SourceLocation getInstanceofLoc() const LLVM_READONLY { return Loc; }
  SourceLocation getLocStart() const LLVM_READONLY { return Loc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return Loc; }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpInstanceofExprClass;
  }

  child_range children() { 
    return child_range(Exprs, Exprs + 1 + !!Exprs[1]);
  }
};


// PhpEllipsisExpandExpr: used to represent ...<expr>
//
class PhpEllipsisExpandExpr : public Expr {
  Stmt *Expand;
  SourceLocation Loc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setEllipsisLoc(SourceLocation L) { Loc = L; }

public:
  PhpEllipsisExpandExpr(QualType Ty, SourceLocation EllipsisLoc, Expr *Base);
  PhpEllipsisExpandExpr(EmptyShell Empty) 
    : Expr(PhpEllipsisExpandExprClass, Empty), Expand(nullptr) {}

  Expr *getExpandExpr() const { return static_cast<Expr*>(Expand); }
  void setExpandExpr(Expr *E) { Expand = E; }

  SourceLocation getEllipsisLoc() const LLVM_READONLY { return Loc; }
  SourceLocation getLocStart() const LLVM_READONLY { return Loc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return Expand->getLocEnd(); }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpEllipsisExpandExprClass;
  }

  // Iterators
  child_range children() { return child_range(&Expand, &Expand + 1); }
};


/// \brief Represents using '&' in some expressions.
///
class PhpReferenceExpr : public Expr {
  Stmt *Base;
  SourceLocation Loc;

  friend class ASTStmtReader;
  friend class ASTStmtWriter;

  void setReferenceLoc(SourceLocation L) { Loc = L; }

public:
  PhpReferenceExpr(SourceLocation ReferenceLoc, Expr *Base);
  PhpReferenceExpr(EmptyShell Empty)
    : Expr(PhpReferenceExprClass, Empty), Base(nullptr) {
  }

  Expr *getBaseExpr() const { return static_cast<Expr*>(Base); }
  void setBaseExpr(Expr *E) { Base = E; }

  SourceLocation getReferenceLoc() const LLVM_READONLY { return Loc; }
  SourceLocation getLocStart() const LLVM_READONLY { return Loc; }
  SourceLocation getLocEnd() const LLVM_READONLY { return Base->getLocEnd(); }

  static bool classof(const Stmt *T) {
    return T->getStmtClass() == PhpReferenceExprClass;
  }

  // Iterators
  child_range children() { return child_range(&Base, &Base + 1); }
};

}  // end namespace clang

#endif
