//===--- PhpSupport.h --- PHP support in driver -----------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_DRIVER_PHPSUPPORT_H
#define LLVM_CLANG_DRIVER_PHPSUPPORT_H

//------ Dependencies ----------------------------------------------------------
#include "clang/Driver/Driver.h"
#include "llvm/Option/ArgList.h"
#include "llvm/Option/Option.h"
//------------------------------------------------------------------------------

namespace clang { namespace driver {

using namespace llvm::opt;

std::string getPhpResourcePath(std::string ClangResourceDir);

void addPhpLinkerOptions(const Driver &D, const ArgList &Args,
                         ArgStringList &CmdArgs, bool IsWin);

void addPhpLinkerLibs(const Driver &D, const ArgList &Args,
                      ArgStringList &CmdArgs, bool IsWin);

} }

#endif
