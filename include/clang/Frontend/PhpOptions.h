//===-- PhpOptions.h --------------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Defines class PhpOptions, which represents set of .
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_FRONTEND_PHPOPTIONS_H_
#define LLVM_CLANG_FRONTEND_PHPOPTIONS_H_

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/IntrusiveRefCntPtr.h"
#include <string>
//------------------------------------------------------------------------------

namespace clang {

enum class PhpModuleKind {
  Module,
  EModule,
  Page,
  Script,
  Config
};

enum class PhpDumpStage {
  None,
  Parse,
  Trans
};

enum class PhpThreading {
  Single,
  Multi
};


enum class PhpRewriteMode {
  None
};


class PhpOptions : public llvm::RefCountedBase<PhpOptions> {
public:
  PhpModuleKind Kind;

  std::string Symfile;
  std::string AppDir;

  bool CxxArith;
  PhpThreading Threading;
  PhpRewriteMode RewriteMode;

  PhpDumpStage DumpStage;

  PhpOptions() : Kind(PhpModuleKind::Script),
    CxxArith(false), Threading(PhpThreading::Single),
    RewriteMode(PhpRewriteMode::None), DumpStage(PhpDumpStage::None) {
  }
};

}

#endif
